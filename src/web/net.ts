import * as Swordboard from '../index';
import io from 'socket.io-client';
import EventEmitter from 'events';



export const socket = io();
export const board = new Swordboard.Board({
    client: true,
});
export const checker = new EventEmitter();

let myPlayer: Swordboard.Player | null = null;



export function sendCommand(cmd: string, args: string[]) {
    // console.log(`Sent ${cmd} with ${args.length} arguments`);
    socket.emit('data', `${cmd} ${args.join(' ')}\n`);
}


function received(cmd: string, args: string[]) {
    // console.log('Received ' + cmd);

    if (cmd.toUpperCase() === 'NEWOBJ') {
        checker.emit('new object', Swordboard.BoardObject.load(board, args.join(' ')));
    }

    if (cmd.toUpperCase() === 'CLEAN') {
        checker.emit('clean');
        board.clean();
    }

    if (cmd.toUpperCase() === 'DELOBJ') {
        if (board.objects.has(args[0]))
            board.objects.get(args[0])!.remove();
            
        checker.emit('deleted object');
    }

    else if (cmd.toUpperCase() === 'NEWPLY') {
        let pl = new Swordboard.Player(board, args[0]);
        board.players.set(pl.id, pl);
    }

    else if (cmd.toUpperCase() === 'TURNS') {
        board.turnIndex = +args[0];
        board.turns = JSON.parse(args.slice(1).join(' '));
        checker.emit('turn');
    }

    else if (cmd.toUpperCase() === 'NEXTTURN') {
        board.turn();
        checker.emit('turn');
    }

    else if (cmd.toUpperCase() === 'ISPLAY') {
        myPlayer = board.players.get(args[0])!;

        if (myPlayer) {
            console.log('Got player!');
            checker.emit('player', myPlayer);
        }
    }

    else if (cmd.toUpperCase() === 'ISOBJC') {
        let playId = args[0];
        let objId = args[1];

        if (board.objects.has(objId) && board.players.has(playId)) {
            board.players.get(playId)!.object = (board.objects.get(objId)! as Swordboard.BoardLiving);

            if (myPlayer && playId == myPlayer.id) {
                if (myPlayer.object) {
                    checker.emit('logged in');
                    
                    myPlayer.object.on('set-health', (heal) => {
                        checker.emit('health', +heal);
                    });

                    myPlayer.object.on('set-stats', (stats) => {
                        checker.emit('stats', JSON.parse(stats));
                    });

                    myPlayer.object.on('set-points', (pts) => {
                        checker.emit('points', +pts);
                    });

                    checker.emit('health', myPlayer.object.health);
                    checker.emit('stats', myPlayer.object.stats);
                    checker.emit('points', myPlayer.object.availablePoints);
                }
            }
        }
    }

    else if (cmd.toUpperCase() === 'OBJCEV') {
        if (board.objects.has(args[0])) {
            // console.log(`Object ${args[0]} got event '${args[1]}'`);
            board.objects.get(args[0])!.emit.apply(board.objects.get(args[0]), ([args[1]].concat(JSON.parse(args.slice(2).join(' '))) as any));
            checker.emit('object event', board.objects.get(args[0]), args[1], JSON.parse(args.slice(2).join(' ')));
        }
    }

    else if (cmd.toUpperCase() === 'PLAYEV') {
        if (board.players.has(args[0])) {
            // console.log(`Player ${args[0]} got event '${args[1]}'`);
            board.players.get(args[0])!.emit.apply(board.players.get(args[0]), ([args[1]].concat(JSON.parse(args.slice(2).join(' '))) as any));
            checker.emit('player event', board.players.get(args[0]), args[1], JSON.parse(args.slice(2).join(' ')));
        }
    }
}


let buffer = '';

socket.on('data', (data: Buffer) => {
    buffer += data.toString('utf-8');
    let lines = buffer.split('\n');
    buffer = lines.slice(-1)[0];

    lines.slice(0, -1).forEach((l) => {
        let tokens = l.split(' ');
        received(tokens[0], tokens.slice(1));
    })
});

socket.on('reconnect', () => {
    socket.emit('data', '!RESTORE\n');
});
