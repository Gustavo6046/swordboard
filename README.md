# Swordboard

_Test your might,_  
_run from plight,_  
_and become you might_  
_a fearsome sight._  


This is a simple online RPG game, written in Node.JS. Its frontend and graphics
are unusually written using Vue (instead of a proper game graphics engine, like
Pixi.js).

Enjoy upgrading your stats, defeating monsters, and enraging PETA. :)

## How to Play

**If a server is already up for you,** you may simply point your browser to
its HTTP frontend. If you aren't self-hosting it, or want to play with more
people, there might be public servers around!

**Otherwise,** use the  `swordboard-server` command to host a server from the
port specified as the first argument. Then connect to `http://localhost:PORT`
using your favorite browser. Et voilá! You're alone, deserted. _(Rick Astley
lied to you!)_
